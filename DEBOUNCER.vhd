----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    09:50:31 10/31/2013 
-- Design Name: 
-- Module Name:    DEBOUNCER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--USE ieee.std_logic_signed.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

--USE ieee.std_logic_signed.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEBOUNCER is
	port (
	clk:		in std_logic;								-- ADC clock
	ADC_in:	in std_logic_vector(16 downto 0);
	ADC_out: out std_logic_vector(15 downto 0));
end DEBOUNCER;

architecture Behavioral of DEBOUNCER is

signal Q1, Q2, adc0 : std_logic_vector(13 downto 0) := (others => '0');
signal delay : std_logic_vector(8 downto 0) := (others => '0');



begin
-- DEBOUNCER & DELAY & extrude signal to 16 bit
	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
			Q1(13 downto 0)<=ADC_in(15 downto 2);
			Q2<=Q1;
			adc0<=Q2;
			ADC_out <= EXT(adc0, 16); -- zero extend
		END IF; -- CLK 
	END PROCESS;

end Behavioral;

