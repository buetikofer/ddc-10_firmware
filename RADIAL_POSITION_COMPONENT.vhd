----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:58:31 02/11/2014 
-- Design Name: 
-- Module Name:    RING_VETO_COMPONENT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RADIAL_POSITION_COMPONENT is
	port (
	clk:			in std_logic;
	ADC_AllCorr:in std_logic_vector(15 downto 0);
	ADC_OuterRing:	in std_logic_vector(16 downto 0);
	ADC_InnerRing:	in std_logic_vector(16 downto 0);
	GetBLine:	in std_logic;
	Initiate:	in std_logic;
	SignalSign:	in std_logic;
	IntWindow:	in integer range 0 to 8000;
	SignalThreshold: in std_logic_vector(15 downto 0);
	OuterRingFactor : in std_logic_vector(15 downto 0);
	InnerRingFactor : in std_logic_vector(15 downto 0);
	RingVetoOut:out std_logic;
	RingVetoTest:out std_logic;
	RingVetoPeakOn:out std_logic;
	baselineOuter:out std_logic_vector(15 downto 0);
	baselineOuter_ok:out std_logic;
	baselineInner:out std_logic_vector(15 downto 0);
	baselineInner_ok:out std_logic
);
end RADIAL_POSITION_COMPONENT;

architecture Behavioral of RADIAL_POSITION_COMPONENT is

component DEBOUNCER
	port (
	clk:		in std_logic;								-- ADC clock
	ADC_in:	in std_logic_vector(16 downto 0);
	ADC_out: out std_logic_vector(15 downto 0));
end component;

----------- BASELINE COMPONENT ---------
component BLINE
	port (
	clk:			in std_logic;								-- ADC clock
	getBline:	in std_logic;
	initiate : in std_logic;
	signal_sign:in std_logic;
	ADC_in:		in std_logic_vector(15 downto 0);
	ADC_out:		out std_logic_vector(15 downto 0);
	baseline: 	out std_logic_vector(15 downto 0);
	baseline_ok:out std_logic
);
end component;


--signals
signal ADC_OuterRingDEB: std_logic_vector(15 downto 0);
signal ADC_InnerRingDEB: std_logic_vector(15 downto 0);
signal ADC_OuterRingCorr: std_logic_vector(15 downto 0);
signal ADC_InnerRingCorr: std_logic_vector(15 downto 0);
signal baseline : std_logic_vector(15 downto 0);
--signal baselineOuter_ok : std_logic;
--signal baselineInner_ok : std_logic;
signal SumCounter: integer range 0 to 8000 := 0;
signal PeakOn: std_logic := '0';

signal OuterSum : std_logic_vector(21 downto 0) := (others => '0');
signal InnerSum : std_logic_vector(21 downto 0) := (others => '0');


begin

	DEB_OuterRing: DEBOUNCER
		port map(
			clk=>clk,
			ADC_in=>ADC_OuterRing,
			ADC_out=>ADC_OuterRingDEB);
			
	DEB_InnerRing: DEBOUNCER
		port map(
			clk=>clk,
			ADC_in=>ADC_InnerRing,
			ADC_out=>ADC_InnerRingDEB);
			
	BLINE_Outer : BLINE
		PORT MAP (
			clk=>clk,
			getBline=>GetBLine, -- get baseline when ctrl_reg(2) := '1'
			initiate=>Initiate, -- get baseline when initiation is '1'
			signal_sign=>SignalSign,
			ADC_in=>ADC_OuterRingDEB,
			ADC_out=>ADC_OuterRingCorr,
			baseline=>baselineOuter,
			baseline_ok=>baselineOuter_ok
			);

	BLINE_Inner : BLINE
		PORT MAP (
			clk=>clk,
			getBline=>GetBLine, -- get baseline when ctrl_reg(2) := '1'
			initiate=>Initiate, -- get baseline when initiation is '1'
			signal_sign=>SignalSign,
			ADC_in=>ADC_InnerRingDEB,
			ADC_out=>ADC_InnerRingCorr,
			baseline=>baselineInner,
			baseline_ok=>baselineInner_ok
			);

			
			
-----------------------------------
---------- Ring Veto --------------
-----------------------------------
	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
		
			-- Looking for a peak to start
			--IF ADC_AllCorr < (-SignalThreshold) AND PeakOn = '0' THEN -- peak starting edge is found (in sum signal)
			IF ADC_OuterRingCorr + ADC_InnerRingCorr < (-SignalThreshold) AND PeakOn = '0' THEN -- only peaks are taken into account that have signal in (InnerRing + OuterRing)... does SignalThreshold work???
				PeakOn <= '1';
				RingVetoPeakOn <= '1';
				SumCounter <= 0;
				OuterSum <= (others => '0');
				InnerSum <= (others => '0');
			END IF;
			
			-- IF Peak is found, start to sum up (integrate) inner and outer ring signal
			IF SumCounter<IntWindow AND PeakOn = '1' THEN -- if peak starting edge is found, starting to sum up outer and inner Ring signal
				OuterSum <= OuterSum + ADC_OuterRingCorr;
				InnerSum <= InnerSum + ADC_InnerRingCorr;
				SumCounter <= SumCounter + 1;
			END IF;
			
			-- After IntWindow check ratio of outer/inner ring
			IF SumCounter=IntWindow THEN 
				IF OuterRingFactor*OuterSum < InnerRingFactor*InnerSum THEN -- might not work for smal signals?! => threshold on OuterRing needed...
					RingVetoOut <= '1'; -- is on until the next peak is found
					RingVetoTest <= '1';
				END IF;
				SumCounter <= SumCounter + 1;
			END IF;
			
			-- hold information for Veto component for 10 steps
			IF SumCounter > IntWindow AND SumCounter < IntWindow+10 THEN
				SumCounter <= SumCounter+1;
			END IF;
			
			-- clear veto signal of RingVeto
			IF SumCounter=IntWindow+10 THEN
				RingVetoOut <= '0';
				RingVetoTest <= '0';
				RingVetoPeakOn <= '0';				
				PeakOn <= '0'; -- ready for new peak (IntWindow should be longer than peak!)
			END IF;
			
		END IF; -- CLK 
	END PROCESS;
end Behavioral;

