----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:13:59 02/11/2014 
-- Design Name: 
-- Module Name:    HE_VETO_COMPONENT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PEAK_INFO_COMPONENT is
	port (
	clk:			in std_logic;
	ADC_All:	in std_logic_vector(15 downto 0);
	GetBLine:	in std_logic;
	Initiate:	in std_logic;
	Baseline:	in std_logic_vector(15 downto 0);
	BaselineOK: in std_logic;
	SignalSign:	in std_logic;
	IntWindow:	in integer range 0 to 8000;
	SignalThreshold: in std_logic_vector(15 downto 0);
	PeakStart:	in std_logic;
	PeakIntegral:	out std_logic_vector(23 downto 0);
	PeakWidth:		out integer range 0 to 1000;
	PeakRiseTime:	out integer range 0 to 500
);
end PEAK_INFO_COMPONENT;



architecture Behavioral of PEAK_INFO_COMPONENT is

--component DEBOUNCER
--	port (
--	clk:		in std_logic;								-- ADC clock
--	ADC_in:	in std_logic_vector(16 downto 0);
--	ADC_out: out std_logic_vector(15 downto 0));
--end component;
--
------------- BASELINE COMPONENT ---------
--component BLINE
--	port (
--	clk:			in std_logic;								-- ADC clock
--	getBline:	in std_logic;
--	initiate : in std_logic;
--	signal_sign:in std_logic;
--	ADC_in:		in std_logic_vector(15 downto 0);
--	ADC_out:		out std_logic_vector(15 downto 0);
--	baseline: 	out std_logic_vector(15 downto 0);
--	baseline_ok:out std_logic
--);
--end component;

----------- INTEGRATOR COMPONENT ---------
component INTEGRATOR
	port (
	clk:			in std_logic;								-- ADC clock
	enable:		in std_logic;
	int_window: in integer range 0 to 8000;
	ADC:			in std_logic_vector(15 downto 0);
	baseline:	in std_logic_vector(15 downto 0);
	integral: 	out std_logic_vector(23 downto 0)
);
end component;

----------- WIDTH COMPONENT ---------
component WIDTH_COMPONENT
	port (
	clk:			in std_logic;								-- ADC clock
	baseline_ok: in std_logic;
	peak_start: in std_logic;
	ADC:			in std_logic_vector(15 downto 0);
	signal_threshold: in std_logic_vector(15 downto 0);
--	test4: 	out std_logic;
	width: 		out integer range 0 to 500
);
end component;

----------- RISE TIME COMPONENT ---------
component RISETIME_COMPONENT
	port (
	clk:			in std_logic;								-- ADC clock
	baseline_ok:in std_logic;
	ADC:			in std_logic_vector(15 downto 0);
	signal_threshold: in std_logic_vector(15 downto 0);
	risetime: 		out integer range 0 to 500
);
end component;

-- Width
signal peak_width : integer range 0 to 500;
--signal test4 : std_logic :='0';

-- Rise time
signal peak_risetime : integer range 0 to 500;


begin

--	DEB_DEL: DEBOUNCER
--		port map(
--			clk=>clk,
--			ADC_in=>ADC_All,
--			ADC_out=>ADC_AllDEB);
--			
--	BLine_calculator : BLINEs
--		PORT MAP (
--			clk=>clk,
--			getBline=>GetBLine, -- get baseline when ctrl_reg(2) := '1'
--			initiate=>Initiate, -- get baseline when initiation is '1'
--			signal_sign=>SignalSign,
--			ADC_in=>ADC_AllDEB,
--			ADC_out=>ADC_AllCorr,
--			baseline=>baseline,
--			baseline_ok=>baseline_ok
--			);
			
	ADC_integral : INTEGRATOR 
		PORT MAP (
			clk=>clk,
			enable=>BaselineOk,
			int_window=>IntWindow,
			ADC=>ADC_All,
			baseline=>Baseline,
			integral=>PeakIntegral
		);
		
	width : WIDTH_COMPONENT
	PORT MAP (
		clk=>clk,
		baseline_ok=>BaselineOk,
		peak_start=>PeakStart,
		ADC=>ADC_All,
		signal_threshold=>SignalThreshold,
--		test4=>test4,
		width=>PeakWidth
	);

	risetime : RISETIME_COMPONENT
	PORT MAP (
		clk=>clk,
		baseline_ok=>BaselineOk,
		ADC=>ADC_All,
		signal_threshold=>SignalThreshold,
		risetime=>PeakRiseTime
	);
			

			


end Behavioral;

