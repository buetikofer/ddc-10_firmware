----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    10:55:28 11/15/2013 
-- Design Name: 
-- Module Name:    INITIATE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;
use IEEE.std_logic_arith.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity STATUS_COMPONENT is
	PORT (
		CLK_ADC	: in STD_LOGIC;
		get_status : in std_logic;
		input_b : out std_logic_vector(15 downto 0);
		enable_b : out std_logic;
		wrenable_b : out std_logic_vector(0 downto 0);
		MemAddr_ctr : inout STD_LOGIC_VECTOR (12 downto 0);
		-- parameter
		signal_sign		: in std_logic;
		int_window		: in integer range 0 to 50000 := 100;
		veto_delay		: in integer range 0 to 1000 := 400;
		signal_threshold: in std_logic_vector(15 downto 0);
		int_threshold	: in std_logic_vector (31 downto 0);
		width_cut		: in integer range 0 to 1000;
		risetime_cut	: in integer range 0 to 500;
		component_selector: in std_logic_vector(3 downto 0);
		par1 : in std_logic_vector(63 downto 0);
		par2 : in std_logic_vector(63 downto 0);
		par3 : in std_logic_vector(63 downto 0);
		par4 : in std_logic_vector(63 downto 0);
		OuterRingFactor : in std_logic_vector(15 downto 0);
		InnerRingFactor : in std_logic_vector(15 downto 0);
		PreScaling		: in std_logic_vector(15 downto 0);
		baseline			: in std_logic_vector(15 downto 0);
		baselineOuterRing		: in std_logic_vector(15 downto 0);
		baselineInnerRing		: in std_logic_vector(15 downto 0)
		);
end STATUS_COMPONENT;


architecture Behavioral of STATUS_COMPONENT is
TYPE parameter_type IS ARRAY (30 downto 0) OF STD_LOGIC_VECTOR (15 DOWNTO 0);


signal do_status : std_logic :='0';
signal parameter : parameter_type; 
signal counter : integer range 0 to 4096 :=0;
signal address	: std_logic_vector(12 downto 0);


begin
	PROCESS (CLK_ADC) BEGIN
		IF rising_edge (CLK_ADC) THEN
	
		IF signal_sign = '0' THEN
			parameter(0)<=(others => '0');
		ELSE
			parameter(0)<="0000000000000001";
		END IF;	
		parameter(1)<=CONV_STD_LOGIC_VECTOR(int_window, 16);
		parameter(2)<=CONV_STD_LOGIC_VECTOR(veto_delay, 16);
		parameter(3)<=signal_threshold;
		parameter(4)<=int_threshold(31 downto 16);
		parameter(5)<=int_threshold(15 downto 0);
		parameter(6)<=CONV_STD_LOGIC_VECTOR(width_cut, 16);
		parameter(7)<=CONV_STD_LOGIC_VECTOR(risetime_cut, 16);
		parameter(8)<=EXT(component_selector, 16);
		
		parameter(9)<=par1(63 downto 48);
		parameter(10)<=par1(47 downto 32);
		parameter(11)<=par1(31 downto 16);
		parameter(12)<=par1(15 downto 0);
		parameter(13)<=par2(63 downto 48);
		parameter(14)<=par2(47 downto 32);
		parameter(15)<=par2(31 downto 16);
		parameter(16)<=par2(15 downto 0);
		parameter(17)<=par3(63 downto 48);
		parameter(18)<=par3(47 downto 32);
		parameter(19)<=par3(31 downto 16);
		parameter(20)<=par3(15 downto 0);
		parameter(21)<=par4(63 downto 48);
		parameter(22)<=par4(47 downto 32);
		parameter(23)<=par4(31 downto 16);
		parameter(24)<=par4(15 downto 0);
		parameter(25)<=OuterRingFactor;
		parameter(26)<=InnerRingFactor;
		parameter(27)<=PreScaling;
		
		parameter(28)<=baseline;
		parameter(29)<=baselineOuterRing;
		parameter(30)<=baselineInnerRing;

		
			IF get_status = '1' AND do_status = '0' THEN
				do_status<='1';
				MemAddr_ctr<=(others => '0');
				enable_b<='1';
				wrenable_b<="1";
				counter<=1;
				input_b<=parameter(0);
			END IF;
			
			IF do_status = '1' THEN
				MemAddr_ctr<=MemAddr_ctr + 1;
				counter<=counter + 1;
				input_b<=parameter(counter); -- BRAM needs one clk cycle, therefore parameter(counter) coresponds to parameter(counter -1)
				IF counter = 31 THEN
					do_status<='0';
					enable_b<='0';
					wrenable_b<="0";
					MemAddr_ctr<=(others => '0');
				END IF;
			END IF;
			
		END IF; -- CLK 
	END PROCESS;
	
	--parameter(0) contains nothing (waiting for BRAM)
--	<slv_sig> = CONV_STD_LOGIC_VECTOR(<int_sig>, <integer_size>);



end Behavioral;
