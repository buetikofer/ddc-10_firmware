----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    13:42:30 10/29/2013 
-- Design Name: 
-- Module Name:    INTEGRATOR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
--
-- Additional Comments: This component prefomres the integration of the incomming sum signal
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity INTEGRATOR is
	port (
	clk:		in std_logic;								-- ADC clock
	enable:	in std_logic;
	int_window: in integer range 0 to 8000;		-- maximum integration time is 10 us
	ADC:		in std_logic_vector(15 downto 0);
	baseline:in std_logic_vector(15 downto 0);
	integral: out std_logic_vector(23 downto 0)
	);
end INTEGRATOR;

architecture Behavioral of INTEGRATOR is
TYPE ADC_MemBuffer IS ARRAY (1000 downto 0) OF std_logic_vector (15 DOWNTO 0); -- Buffer for ADC samples


--------------- SIGNALS ----------------------
signal counter : integer range 0 to 1000 := 0;
signal adc_buffer : ADC_MemBuffer := (others => "0000000000000000");
signal sum : std_logic_vector (23 downto 0) := (others => '0');
signal minimum : std_logic_vector(23 downto 0) := (others => '0');



begin


	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
		
			IF counter = int_window THEN
				counter<=0;
			ELSE 
				counter<=counter + 1;
			END IF;
			
			adc_buffer(counter) <= ADC;
			sum <= sum + ADC - adc_buffer(counter);
			integral <= sum + ADC - adc_buffer(counter);		
					
		END IF; -- CLK 
	END PROCESS;


end Behavioral;

