----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    13:28:32 11/06/2013 
-- Design Name: 
-- Module Name:    VETO_COMPONENT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this component colects the information about the peaks and makes the veto decision,
--								calculates the veto lenghts and generates the veto output signal
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VETO_COMPONENT is
	port (
	clk:			in std_logic;
	veto_delay: in integer range 0 to 1000;
	ADC_in:		in std_logic_vector(15 downto 0);
	component_selector: in std_logic_vector (3 downto 0);
	width_cut:	in integer range 0 to 1000;
	risetime_cut:in integer range 0 to 500;
	signal_threshold:	in std_logic_vector(15 downto 0);
	threshold:	in std_logic_vector(31 downto 0); -- throshold of integral cut (not signal threshold)
	IntWindow:	in integer range 0 to 8000;
	integral:	in std_logic_vector(23 downto 0);
	width:		in integer range 0 to 1000;
	risetime:	in integer range 0 to 500;
	PositionVeto: in std_logic;
	PreScaling:	in std_logic_vector(15 downto 0);
	par1 : in std_logic_vector(63 downto 0);
	par2 : in std_logic_vector(63 downto 0);
	par3 : in std_logic_vector(63 downto 0);
	par4 : in std_logic_vector(63 downto 0);
	test1 : out std_logic;
	test2 : out std_logic;
	test3 : out std_logic;	
	veto:			out std_logic;
	start_peak: out std_logic;
	start_veto: out std_logic
	);
end VETO_COMPONENT;

architecture Behavioral of VETO_COMPONENT is

-- SIGNALS
signal peak_on : std_logic :='0';

signal veto_start0, veto_start1 : std_logic :='0';
signal veto_OK : std_logic :='0';
signal counter : integer range 0 to 1100000000 := 200;
signal veto_off_time : integer range 0 to 1000 := 0;

signal veto_state : std_logic := '0';
signal veto_length : integer range 0 to 1100000000 := 0;

signal res_temp0, res_temp1 : std_logic_vector(105 downto 0);
signal res_fin0, res_fin1 : std_logic_vector(51 downto 0);

signal PreScaleCounter : std_logic_vector(15 downto 0) := (others => '0');

-- convert par4 to [us]
signal convert_par4 : std_logic_vector(15 downto 0) := "0000000001100100";

-- parameters for peaks
signal integral_scaled0, integral_scaled1 : std_logic_vector(13 downto 0);
signal integral_scaled0_pow2, integral_scaled1_pow2 : std_logic_vector(27 downto 0);
signal integral_scaled0_pow3, integral_scaled1_pow3 : std_logic_vector(41 downto 0);
signal delay_counter0 : integer range 0 to 1100 := 1100;
signal delay_counter1 : integer range 0 to 1100 := 1100;
signal delay_ready0 : std_logic := '1'; -- delay is ready if '1'
signal delay_ready1 : std_logic := '1'; -- delay is ready if '1'

signal veto_decision0 : std_logic_vector(3 downto 0) := "0000"; -- bit3 = RingVeto, bit2 = integral, bit1 width, bit0 risetime
signal veto_decision1 : std_logic_vector(3 downto 0) := "0000"; -- bit3 = RingVeto, bit2 = integral, bit1 width, bit0 risetime
signal width0, width1 : std_logic := '0';

signal delay_select : std_logic := '0';
signal width_ready : std_logic_vector(1 downto 0) := "00";




begin

	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN	
			
			-- check signal is above threshold
			IF ADC_in < (-signal_threshold) THEN
				peak_on<='1';
			ELSE
				peak_on<='0';
			END IF;
			
			-- if signal is above threshold for first time --> start peak (at delay 0)
			IF ADC_in < (-signal_threshold) AND peak_on = '0' AND delay_ready0 = '1'  AND delay_select = '0' THEN
				delay_counter0 <= 0;
				delay_ready0 <= '0';
				delay_select <= '1';
				width_ready(0) <= '1';
			END IF;

			-- if signal is above threshold for first time --> start peak (at delay 1)			
			IF ADC_in < (-signal_threshold) AND peak_on = '0' AND delay_ready1 = '1' AND delay_select = '1' THEN
				delay_counter1 <= 0;
				delay_ready1 <= '0';
				delay_select <= '0';
				width_ready(1) <= '1';
			END IF;

			-- get peak width and risetime whenever a new peak width is found
			IF width > 0 THEN
				-- width belongs to delay 0
				IF width_ready = "01" OR (width_ready = "11" AND delay_counter0 > delay_counter1 AND delay_counter0 < 1000) THEN
					width0<='1';
					width_ready(0)<='0';
					IF width > width_cut THEN
						veto_decision0(1)<='1';
					ELSE 
						veto_decision0(1)<='0';
					END IF;
					IF risetime > risetime_cut THEN
						veto_decision0(0)<='1';
					ELSE 
						veto_decision0(0)<='0';
					END IF;
				-- width belongs to delay 1
				ELSIF width_ready = "10" OR (width_ready = "11" AND delay_counter1 > delay_counter0 AND delay_counter1 < 1000) THEN
					width1<='1';
					width_ready(1) <= '0';
					IF width > width_cut THEN
						veto_decision1(1)<='1';
					ELSE 
						veto_decision1(1)<='0';
					END IF;
					IF risetime > risetime_cut THEN
						veto_decision1(0)<='1';
					ELSE 
						veto_decision1(0)<='0';
					END IF;
				END IF;
			END IF;			
			
			-- VETO DELAY0
			If delay_counter0 < (veto_delay + 1) THEN 
				delay_counter0 <= delay_counter0 + 1;
				IF integral < (-threshold) THEN
					veto_decision0(2)<='1'; -- integral is below integral threshold => integral cut condidtion fullfiled 
					IF integral(23 downto 10) < integral_scaled0 THEN -- getting maximum integral value
						integral_scaled0<=integral(23 downto 10); 
						integral_scaled0_pow2<=(integral(23 downto 10)*integral(23 downto 10));
					END IF;
				END IF;
			END IF;
			IF delay_counter0 = IntWindow+5 THEN -- PositionVeto (ring veto) signal needs one cicle to update, therefore IntWindow+1
				veto_decision0(3)<=PositionVeto;
			END IF;
			-- END OF VETO DELAY0
			IF delay_counter0 = veto_delay THEN
				veto_start0<='1';
				-- veto decision width:
				-- risetime and integral are coupled via logic AND (if components are on)
				-- ring veto is coupled via logic OR to the other components,
				-- but the with has to be bigger than the with threshold in order to only consider S2 peaks for the position veto
				IF (component_selector(0) <= veto_decision0(0) AND component_selector(1) <= veto_decision0(1) AND component_selector(2) <= veto_decision0(2) AND component_selector(2 downto 0)/="000") OR (component_selector(3) = '1' AND veto_decision0(3) = '1' AND veto_decision0(1) = '1') THEN ---
					IF PreScaleCounter < (PreScaling-1) OR PreScaling = 0 THEN
						PreScaleCounter<=PreScaleCounter + 1;
						veto_OK<='1';
--						res_temp0<=par1*(-integral_scaled0)*integral_scaled0*integral_scaled0 + par2*integral_scaled0*integral_scaled0 + par3*(-integral_scaled0) + par4*convert_par4; -- convert par4 to [us] -- doesn't work well
						res_temp0<=("00000000000000" & (par2*integral_scaled0_pow2)) + ("0000000000000000000000000000" & (par3*(-integral_scaled0))) + ("00000000000000000000000000" & (par4*convert_par4)); -- new only 2. order polynomial function
					END IF;
					IF PreScaleCounter >= (PreScaling-1) THEN
						PreScaleCounter<=(others => '0');						
					END IF;
				END IF;
			END IF;
			IF delay_counter0 = (veto_delay + 1) THEN 
				delay_counter0 <= 1000;
				veto_start0 <= '0';
				veto_decision0 <= "0000";
				width0 <= '0';
				res_temp0 <= (others => '0');
				integral_scaled0 <= (others => '0');
				delay_ready0 <= '1';
				width_ready(0) <= '0';
			END IF;

			-- VETO DELAY1
			If delay_counter1 < (veto_delay + 1) THEN 
				delay_counter1<=delay_counter1 + 1;
				IF integral < (-threshold) THEN 
					veto_decision1(2)<='1'; -- integral is below integral threshold => integral cut condidtion fullfiled 
					IF integral(23 downto 10) < integral_scaled1 THEN -- getting maximum integral value
						integral_scaled1<=integral(23 downto 10);
						integral_scaled1_pow2<=(integral(23 downto 10)*integral(23 downto 10));
					END IF;
				END IF;
			END IF;
			IF delay_counter1 = IntWindow+5 THEN -- PositionVeto signal needs one cicle to update, therefore IntWindow+1
				veto_decision1(3)<=PositionVeto;
			END IF;
			-- END OF VETO DELAY1			
			IF delay_counter1 = veto_delay THEN 
				veto_start1<='1';
				-- veto decision width:
				-- risetime and integral are coupled via logic AND (if components are on)
				-- ring veto is coupled via logic OR to the other components,
				-- but the with has to be bigger than the with threshold in order to only consider S2 peaks for the position veto

				IF (component_selector(0) <= veto_decision1(0) AND component_selector(1) <= veto_decision1(1) AND component_selector(2) <= veto_decision1(2) AND component_selector(2 downto 0)/="000") OR (component_selector(3) = '1' AND veto_decision1(3) = '1' AND veto_decision1(1) = '1') THEN -- OR (component_selector(3) = '1' AND veto_decision1(3) = '1' AND veto_decision1(1) = '1')
					IF PreScaleCounter < (PreScaling-1) OR PreScaling = 0 THEN
						PreScaleCounter<=PreScaleCounter + 1;
						veto_OK<='1';
--						res_temp1<=par1*(-integral_scaled1)*integral_scaled1*integral_scaled1 + par2*integral_scaled1*integral_scaled1 + par3*(-integral_scaled1) + par4*convert_par4; -- convert par4 to [us] --doesn't work well
						res_temp1<=("00000000000000" & (par2*integral_scaled1_pow2)) + ("0000000000000000000000000000" & (par3*(-integral_scaled1))) + ("00000000000000000000000000" & (par4*convert_par4)); -- new only 2. order polynomial function
					END IF;
					IF PreScaleCounter >= (PreScaling-1) THEN
						PreScaleCounter<=(others => '0');
					END IF;
				END IF;
			END IF;
			IF delay_counter1 = (veto_delay + 1) THEN 
				delay_counter1 <= 1000;
				veto_start1<='0';
				veto_decision1 <= "0000";
				width1<='0';
				res_temp1<=(others => '0');
				integral_scaled1<=(others => '0');
				delay_ready1<='1';
				width_ready(1)<='0';
			END IF;


-----------------------------------
-- veto_length function of integral/1024
-----------------------------------

			-- veto_OK waits for veto_start in order to initiate the veto always at the same time
			IF veto_OK ='1' THEN
				counter<=0;
				veto_OK<='0';
				IF res_temp0 > 0 THEN
					veto_length <= CONV_INTEGER(res_temp0(77 downto 48)); -- get value of polynomial function -- was res_temp0(105 downto 48)
				ELSIF res_temp1 > 0 THEN
					veto_length <= CONV_INTEGER(res_temp1(77 downto 48)); -- get value of polynomial function
				END IF;
			END IF;

			-- veto is active while counter is below 150
			IF counter < veto_length THEN 
--				IF veto_length > 100000 THEN -- could add max veto length here
--					veto_length <= 100000;
--				END IF;
				veto<='1';
				veto_state <= '1';
				counter<=counter + 1; -- if veto length is larger than maximum of counter, counter will wrap araund...
			ELSIF counter = veto_length THEN
--				counter<=1000000000; -- to prevent of veto due to change of 'veto_length' (this should not be necessary anymore and could cause problems?)
				veto<='0';
				veto_state <= '0';
				veto_off_time <= 0;
			ELSE 
				veto<='0';
				veto_state <= '0';
			END IF;
					
		END IF; -- CLK 
	END PROCESS;	

	start_peak<=peak_on;
	start_veto <= veto_start0 OR veto_start1;
	test1<=width0;
	test2<=width1;
	--test3<=;

end Behavioral;
