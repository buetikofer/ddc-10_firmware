----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:16:23 01/09/2014 
-- Design Name: 
-- Module Name:    STATUS_LED_COMPONENT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity STATUS_LED_COMPONENT is
	port (
	clk:			in std_logic;
	initiate:	in std_logic;
	LED_TestFlash: in std_logic;
	RingVetoIn: in std_logic;
	HEvetoIn:	in std_logic;
	InputSignal:  in std_logic_vector(15 downto 0); -- uncorrected sum signal (INPUT 0)
	component_selector: in std_logic_vector(3 downto 0);
	SumSigPeakOn: in std_logic;
	RingVetoPeakOn: in std_logic;
	Saturation: out std_logic;
	LED_out:		out std_logic_vector(3 downto 0)
);

end STATUS_LED_COMPONENT;

architecture Behavioral of STATUS_LED_COMPONENT is
signal counter : integer range 0 to 300000000 := 0;
signal initiate_signal : std_logic := '0';
signal test_flash_signal : std_logic := '0';


begin

	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
					
			IF initiate = '1' THEN
				initiate_signal <= '1';
				counter<=0;
			END IF;
			IF LED_TestFlash = '1' THEN
				test_flash_signal <= '1';
				counter<=0;
			END IF;
			IF initiate_signal = '1' OR test_flash_signal = '1' THEN
				counter<=counter+1;
				IF counter = 0 OR counter = 50000000 OR counter = 100000000 OR counter = 150000000 OR counter = 200000000 THEN
					LED_out<="1111";
				ELSIF counter = 25000000 or counter = 75000000 OR counter = 125000000 OR counter = 175000000 OR counter = 225000000 THEN
					LED_out<="0000";
				END IF;
				IF counter = 225000000 THEN
					initiate_signal <= '0';
					test_flash_signal <= '0';
				END IF;
			END IF;
			
			-- other led status signals			
			IF initiate_signal = '0' AND test_flash_signal = '0' THEN
				LED_out(0) <= component_selector(0);
				LED_out(1) <= component_selector(1);
				LED_out(2) <= component_selector(2);
				LED_out(3) <= component_selector(3);
			END IF;
			
			-- if sum signal is saturated => NIM out 0 is on 
			IF InputSignal = 0 THEN
				Saturation <= '1';
			ELSE
				Saturation <= '0';
			END IF;
					
					
		END IF; -- CLK 
	END PROCESS;


end Behavioral;

