----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:		11.5.2013
-- Design Name: 
-- Module Name:		HE_veto1_3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 	ISE 12.4
--
-- New i.t. version:	Veto length does depend on signal (look up table)
--
--
-- Description:		This firmware calculates the baseline (register 0), corrects for it and 
--							does a continuous integration of the ADC signal over a time window of 1280 ns.
--							If the integral is above the threshold a veto (NIM_OUT(0) and LED_OUT(0) ) of various length is created.
--							The length does depend on the integral of the peak.
--							The veto starts allways at the same time with respect to the peak (~ 1.6 us after peak start).
--
--							The threshold is adjustable through Register(5 downto 1), which will change the threshold 
--							as following:		threshold="XXXXX000000000000000". Where X comes from the register. 
--
--							Remarks: - The threshold should be a positive number => Register(5) should be '0'
--										
--
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;
--USE ieee.std_logic_unsigned.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HE_veto1_3 is
PORT (
	--timecounter : inout std_logic_vector (3 downto 0) := (others => '0');
   	CLK_BF	: in   STD_LOGIC; -- clock from BF microprocessor (120 MHz)
	CLK_100MHZ	: in   STD_LOGIC; -- Main clock 100 MHz.
	ADC_PCB_CLK : in STD_LOGIC; -- return ADC clock ADC E
	BF_PCB_CLK : in STD_LOGIC;
 
	--
	DIP_SWITCH	: in   STD_LOGIC_VECTOR(2 downto 0);	-- DIP switch, lowest three
	NIM_IN      : in   STD_LOGIC_VECTOR(3 downto 0);	-- NIM inputs
	NIM_OUT     : out  STD_LOGIC_VECTOR(3 downto 0) := "0000";	-- NIM outputs	
	LED         : out  STD_LOGIC_VECTOR(3 downto 0) := "0000";	-- Front panel LED's
	LED_ADC_CLK	: out  STD_LOGIC := '0'; -- Diag: connect to ADC PLL lock (not front panel)
	LED_BF_CLK	: out  STD_LOGIC := '0'; -- Diag: connect to BF  PLL lock (not front panel)
	
	
--	testQ1 : out std_logic_vector(13 downto 0);
--	testQ2 : out std_logic_vector(13 downto 0);
--	testadc_16 : out std_logic_vector(15 downto 0);
--	testadc0 : out std_logic_vector(16 downto 0);
--	testtrigger : out std_logic;
--	testADC_armed : out std_logic;
--	testrec : out std_logic;
--	testinput_b : out std_logic_vector(15 downto 0);
--	test_integrated_sig : out std_logic_vector(21 downto 0);
--	test_baseline : out std_logic_vector(15 downto 0);
--	test_baseline_ok : out std_logic;
--	testadc_16_corr : out std_logic_vector(15 downto 0);
	
	--
	-- One general-purpose GPIO pin between the FPGA and the BF processor
	BF_PF1	   : inout  STD_LOGIC;	-- Blackfin PF1 / SPISEL_1 / TMR1
	-- SPI select pins PF4, 5, 6, 7 are also connected
	-- Thirty-two more GPIO's are connected as BF_PPI_0 and BF_PPI_1. See the UCF file.
	--
	-- FPGA is connected as BF external memory
	-- Memory strobes are active LOW (marked with suffix "_b" after the names)
	BF_AWE_b		: in STD_LOGIC;	-- write strobe,  AWE#
	BF_ARE_b		: in STD_LOGIC;	-- read strobe,   ARE#
	BF_AMS0_b	: in STD_LOGIC;	-- AMS0: asynch bank 0 0x2000 0000
	BF_AMS1_b	: in STD_LOGIC;	-- AMS1: asynch bank 1 0x2400 0000

	-- When BF memory is configured in 32-bit mode then ADDR(1) is not needed
	-- All BF addresses are on the 4-byte grid (i.e., 32-bit).
	--
	-- When BF memory is configured in 16-bit mode then ADDR(1) is defined in UCF.
	-- All BF addresses are on the 2-byte grid (i.e., 16-bit).
	--
	-- AMS0 and AMS1 can be be independently configured 16 or 32 bits.
	-- BF_DATA should be "inout", but for testing it is declared it "in"
	BF_DATA	: inout 	STD_LOGIC_VECTOR(31 downto 0); -- BF data bus
	BF_ADDR	: in	STD_LOGIC_VECTOR(25 downto 1); -- BF addr bus

	-- SPI  on the main board  --
	BF_SPISEL_4 : in STD_LOGIC; -- BF_PF4 --> ADC  chips
	BF_SPISEL_5 : in STD_LOGIC; -- BF_PF5 --> ADC  chips
	XSPI_MISO : in STD_LOGIC;
	XSPI_MOSI : in STD_LOGIC;
	XSPI_CLK : in STD_LOGIC;
	XSPI_CS_B : in STD_LOGIC;

	UNUSED_MISO : in STD_LOGIC;
	UNUSED_MOSI : in STD_LOGIC;
	UNUSED_CLK : in STD_LOGIC;
	UNUSED_CS_B : in STD_LOGIC;

	-- ADC channel connections
	AD0_A : in   STD_LOGIC_VECTOR(16 downto 0);
--	AD0_A : in   unsigned(16 downto 0);
	AD0_B : in   STD_LOGIC_VECTOR(16 downto 0);
	AD1_A : in   STD_LOGIC_VECTOR(16 downto 0);
	AD1_B : in   STD_LOGIC_VECTOR(16 downto 0);
	AD2_A : in   STD_LOGIC_VECTOR(16 downto 0);
	AD2_B : in   STD_LOGIC_VECTOR(16 downto 0);
	AD3_A : in   STD_LOGIC_VECTOR(16 downto 0);
	AD3_B : in   STD_LOGIC_VECTOR(16 downto 0);
	AD4_A : in   STD_LOGIC_VECTOR(16 downto 0);
	AD4_B : in   STD_LOGIC_VECTOR(16 downto 0);

	-- connection to DAC
	TXDAC : in STD_LOGIC_VECTOR(13 downto 0);
	TXDAC_CLK : in STD_LOGIC; -- digital synthesis TxDAC clock
	TXDAC_RST : in STD_LOGIC; -- TxDAC reset / pin mode
	TXDAC_SDIO : in STD_LOGIC; -- TXDAC SPI data
	TXDAC_SCK : in STD_LOGIC; -- TXDAX SPI clock
	TXDAC_CS_B : in STD_LOGIC; --
	
	-- ??? 
	AD0_CS_B : in STD_LOGIC;
	AD1_CS_B : in STD_LOGIC;
	AD2_CS_B : in STD_LOGIC;
	AD3_CS_B : in STD_LOGIC;
	AD4_CS_B : in STD_LOGIC;
	ADC_SCK : in STD_LOGIC;
	ADC_SDIO : in STD_LOGIC;
	SDAC_SDI : in STD_LOGIC; --SDAC SPI data to SDAC
	SDAC_1_LOAD : in STD_LOGIC;
	SDAC_0_LOAD : in STD_LOGIC;
	SDAC_SCK : in STD_LOGIC;
	SDAC_PRESET_B : in STD_LOGIC -- preset both SDACs
	
	
  );
end HE_veto1_3;
architecture Behavioral of HE_veto1_3 is

------------ TYPE DECLARATION ----------------
----------------------------------------------
TYPE BF_MemBus_t IS ARRAY (3 downto 0) OF STD_LOGIC_VECTOR (31 DOWNTO 0);
TYPE integral_bus_type IS ARRAY (1999 downto 0) OF STD_LOGIC_VECTOR (15 DOWNTO 0);


--------------- COMPONENTS -------------------
----------------------------------------------

----------- BRAM -----------
component BRAM
	port (
	-- PORT A, BF side	
	clka:		in std_logic;								-- BF clock (?)
	ena: 		in std_logic;								-- probably MemSelect(...)
	wea:		in std_logic_vector(0 downto 0);		-- BF_WRENA0 (transform into std vector)
	addra: 	in std_logic_vector(11 downto 0);	-- BF_ADDR(13 downto 2) ? 2^12=4k times 32 bits
	dina: 	in std_logic_vector(31 downto 0);	-- BF_DATA(31 downto 0)
	douta: 	out std_logic_vector(31 downto 0);	-- Multiplexer
	-- PORT B, FPGA side
	clkb: 	in std_logic;								-- ADC_PCB_CLK ?
	enb: 		in std_logic;								-- ?
	web: 		in std_logic_vector(0 downto 0);		-- create own write enable
	addrb: 	in std_logic_vector(12 downto 0);	-- create own addr 2^13=8k times 16 bits
	dinb: 	in std_logic_vector(15 downto 0);	-- ?
	doutb: 	out std_logic_vector(15 downto 0));	-- ?
end component;
-- Synplicity black box declaration
attribute syn_black_box : boolean;
attribute syn_black_box of BRAM: component is true;

----------- CONTROL REGISTER -----------
COMPONENT CTRL_reg_CPU_writes
	GENERIC (regWdt : INTEGER := 16);
	PORT (
	CLK	: in STD_LOGIC;
	CS	: in STD_LOGIC;
	WR	: in STD_LOGIC;
	RD	: in STD_LOGIC;
	IOBUS	: inout STD_LOGIC_VECTOR (regWdt-1 downto 0);
	regout	: out STD_LOGIC_VECTOR (regWdt-1 downto 0));	-- output
END COMPONENT;

----------- INITIATE COMPONENT -----------
COMPONENT INITIATE_COMPONENT
	PORT (
		CLK_ADC	: in STD_LOGIC;
		initiate : in std_logic;
		output_b : in std_logic_vector(15 downto 0);
		enable_b : out std_logic;
		MemAddr_ctr : inout STD_LOGIC_VECTOR (12 downto 0);
		-- parameter
		signal_sign		: out std_logic;
		int_window		: out std_logic_vector (15 downto 0);
		veto_delay		: out std_logic_vector (15 downto 0);
		signal_threshold: out std_logic_vector (15 downto 0);
		int_threshold	: out std_logic_vector (31 downto 0); -- maybe not enough bits
		width_cut		: out std_logic_vector (15 downto 0);
		risetime_cut	: out std_logic_vector (15 downto 0);
		component_selector : out std_logic_vector(15 downto 0);
		par1 : out std_logic_vector(63 downto 0);
		par2 : out std_logic_vector(63 downto 0);
		par3 : out std_logic_vector(63 downto 0);
		par4 : out std_logic_vector(63 downto 0);
		OuterRingFactor : out std_logic_vector(15 downto 0);
		InnerRingFactor : out std_logic_vector(15 downto 0);
		PreScaling : out std_logic_vector(15 downto 0)
		);
END COMPONENT;



----------- GET VETO STATUS COMPONENT -----------
COMPONENT STATUS_COMPONENT
	PORT (
		CLK_ADC	: in STD_LOGIC;
		get_status : in std_logic;
		input_b : out std_logic_vector(15 downto 0);
		enable_b : out std_logic;
		wrenable_b : out std_logic_vector(0 downto 0);
		MemAddr_ctr : inout STD_LOGIC_VECTOR (12 downto 0);
		-- parameter
		signal_sign		: in std_logic;
		int_window		: in integer range 0 to 50000 := 100;
		veto_delay		: in integer range 0 to 1000 := 400;
		signal_threshold: in std_logic_vector(15 downto 0);
		int_threshold	: in std_logic_vector (31 downto 0);
		width_cut		: in integer range 0 to 500;
		risetime_cut	: in integer range 0 to 500;
		component_selector: in std_logic_vector(3 downto 0);
		par1 : in std_logic_vector(63 downto 0);
		par2 : in std_logic_vector(63 downto 0);
		par3 : in std_logic_vector(63 downto 0);
		par4 : in std_logic_vector(63 downto 0);
		OuterRingFactor : in std_logic_vector(15 downto 0);
		InnerRingFactor : in std_logic_vector(15 downto 0);
		PreScaling		: in std_logic_vector(15 downto 0);		
		baseline			: in std_logic_vector(15 downto 0);
		baselineOuterRing		: in std_logic_vector(15 downto 0);
		baselineInnerRing		: in std_logic_vector(15 downto 0)
		);
END COMPONENT;

----------- DEBOUNC AND DELAY COMPONENT ---------
component DEBOUNCER
	port (
	clk:		in std_logic;								-- ADC clock
	ADC_in:	in std_logic_vector(16 downto 0);
	ADC_out: out std_logic_vector(15 downto 0));
end component;

----------- BASELINE COMPONENT ---------
component BLINE
	port (
	clk:			in std_logic;								-- ADC clock
	getBline:	in std_logic;
	initiate : in std_logic;
	signal_sign:in std_logic;
	ADC_in:		in std_logic_vector(15 downto 0);
	ADC_out:		out std_logic_vector(15 downto 0);
	baseline: 	out std_logic_vector(15 downto 0);
	baseline_ok:out std_logic
);
end component;

----------- PEAK INFO COMPONENT ---------
component PEAK_INFO_COMPONENT
	port (
	clk:			in std_logic;
	ADC_All:	in std_logic_vector(15 downto 0);
	GetBLine:	in std_logic;
	Initiate:	in std_logic;
	Baseline:	in std_logic_vector(15 downto 0);
	BaselineOK: in std_logic;	
	SignalSign:	in std_logic;
	IntWindow:	in integer range 0 to 8000;
	SignalThreshold: in std_logic_vector(15 downto 0);
	PeakStart:	in std_logic;
	PeakIntegral:	out std_logic_vector(23 downto 0);
	PeakWidth:		out integer range 0 to 1000;
	PeakRiseTime:	out integer range 0 to 500
	);
end component;

----------- RING VETO COMPONENT ---------
component RADIAL_POSITION_COMPONENT
	port (
	clk:			in std_logic;
	ADC_AllCorr:in std_logic_vector(15 downto 0);
	ADC_OuterRing:	in std_logic_vector(16 downto 0);
	ADC_InnerRing:	in std_logic_vector(16 downto 0);
	GetBLine:	in std_logic;
	Initiate:	in std_logic;
	SignalSign:	in std_logic;
	IntWindow:	in integer range 0 to 8000;	
	SignalThreshold: in std_logic_vector(15 downto 0);
	OuterRingFactor : in std_logic_vector(15 downto 0);
	InnerRingFactor : in std_logic_vector(15 downto 0);
	RingVetoOut:out std_logic;
	RingVetoTest:out std_logic;
	RingVetoPeakOn:out std_logic;
	baselineOuter:out std_logic_vector(15 downto 0);
	baselineOuter_ok:out std_logic;
	baselineInner:out std_logic_vector(15 downto 0);
	baselineInner_ok:out std_logic
);
end component;

----------- VETO COMPONENT ---------
component VETO_COMPONENT
	port (
	clk:			in std_logic;
	veto_delay: in integer range 0 to 1000;
	ADC_in:		in std_logic_vector(15 downto 0);
	component_selector: in std_logic_vector(3 downto 0);
	width_cut:	in integer range 0 to 1000;
	risetime_cut:in integer range 0 to 500;
	signal_threshold:	in std_logic_vector(15 downto 0);
	threshold:	in std_logic_vector(31 downto 0);
	IntWindow:	in integer range 0 to 8000;
	integral:	in std_logic_vector(23 downto 0);
	width:		in integer range 0 to 1000;
	risetime:	in integer range 0 to 500;
	PositionVeto: in std_logic;
	PreScaling:	in std_logic_vector(15 downto 0);	
	par1 : in std_logic_vector(63 downto 0);
	par2 : in std_logic_vector(63 downto 0);
	par3 : in std_logic_vector(63 downto 0);
	par4 : in std_logic_vector(63 downto 0);
	test1 : out std_logic;
	test2 : out std_logic;	
	test3 : out std_logic;	
	veto:			out std_logic;
	start_peak: out std_logic;
	start_veto: out std_logic
	);
end component;


----------- STATUS LED COMPONENT ---------
component STATUS_LED_COMPONENT
	port (
	clk:			in std_logic;
	initiate:	in std_logic;
	LED_TestFlash:	in std_logic;
	RingVetoIn: in std_logic;
	HEvetoIn:	in std_logic;
	InputSignal:  in std_logic_vector(15 downto 0); 
	component_selector: in std_logic_vector(3 downto 0);
	SumSigPeakOn : in std_logic;
	RingVetoPeakOn: in std_logic;
	Saturation: in std_logic;
	LED_out:		out std_logic_vector(3 downto 0)
);
end component;

--------------- SIGNALS ----------------------
----------------------------------------------

-- BRAM signals
signal BF_AMS1, BF_AMS0 : std_logic; -- BF is using AMS0 or AMS1
signal BF_WR, BF_RD : std_logic;	-- BF is reading/writing
signal BF_rena0, BF_rena1 : std_logic; -- read enable for AMS0/AMS1
signal BF_WRENA0, BF_WRENA1 : std_logic; -- write enable for AMS0/AMS1
signal BF_WRENA0_v, BF_WRENA1_v : std_logic_vector (0 downto 0); 
signal MemSelect : std_logic_vector (3 downto 0); 
signal MemorySpace0 : std_logic_vector (31 downto 0);

SIGNAL MemAddr_ctr: STD_LOGIC_VECTOR (12 downto 0); -- declaration 4k*32 bits
SIGNAL input_b : std_logic_vector(15 downto 0); 
SIGNAL output_b : std_logic_vector(15 downto 0);
signal wrenable_b : std_logic_vector(0 downto 0):="0";
signal enable_b : std_logic :='0';
SIGNAL BF_MemBus : BF_MemBus_t; -- array of BF memory outputs, 32 bits each.

signal enableB : std_logic :='0';
signal wenableB : std_logic :='0';
signal addrB : std_logic_vector (12 downto 0);
signal fpga_in : std_logic_vector (15 downto 0);
signal fpga_out : std_logic_vector (15 downto 0);

-- CONTROL REGISTER
signal RegSelect : std_logic_vector (3 downto 0);
signal RawSelect : std_logic_vector (3 downto 0);
signal BF_ctrl_Reg : std_logic_vector(15 downto 0);

-- INITIATE 
signal initiate : std_logic := '0';
signal address_initiate : std_logic_vector(12 downto 0);
signal enable_initiate : std_logic;

-- GET STATUS
signal get_status : std_logic := '0';
signal address_status : std_logic_vector(12 downto 0);
signal enable_status : std_logic;

-- RECORDER signals
signal get_event : std_logic :='0';
signal get_background : std_logic := '0';
signal q1, q2, q3, adc_deb : std_logic_vector (15 downto 0);

-- DEBOUNCER
signal adc0_16 :  std_logic_vector(15 downto 0) := (others => '0');

-- BASELINE
signal baseline : std_logic_vector(15 downto 0);
signal get_baseline : std_logic :='0';
signal baseline_ok	: std_logic;
signal baseline_error : std_logic;
signal adc0_16_corr :  std_logic_vector(15 downto 0);

-- INTEGRAL
signal integrated_sig : std_logic_vector(23 downto 0);

-- WIDTH
signal peak_width : integer range 0 to 1000;

-- RISE TIME
signal peak_risetime : integer range 0 to 500;

-- VETO
signal veto_out : std_logic;
signal start_veto : std_logic;
signal start_peak : std_logic;
signal test1, test2, test3 : std_logic;
signal veto_error : std_logic;

-- STATUS LED
signal LED_out : std_logic_vector(3 downto 0);
signal LED_TestFlash : std_logic;
signal Saturation : std_logic;

-- RING VETO
signal RingVetoOut : std_logic;
signal RingVetoTest : std_logic :='0';
signal RingVetoPeakOn : std_logic := '0';
signal baselineOuter : std_logic_vector(15 downto 0);
signal baselineOuter_ok : std_logic;
signal baselineInner : std_logic_vector(15 downto 0);
signal baselineInner_ok : std_logic;

--====== INPUT FROM CONTROL FILE ======--
signal int_window_vec : std_logic_vector(15 downto 0);
signal veto_delay_vec : std_logic_vector(15 downto 0);
signal width_cut_vec : std_logic_vector(15 downto 0);
signal risetime_cut_vec : std_logic_vector(15 downto 0);
signal component_selector_vec : std_logic_vector(15 downto 0);

signal int_window: integer range 0 to 50000 := 100;
signal veto_delay: integer range 0 to 1000 := 400;
signal int_threshold: std_logic_vector(31 downto 0) :=(others => '0');
signal width_cut : integer range 0 to 1000;
signal risetime_cut : integer range 0 to 500;
signal component_selector : std_logic_vector (3 downto 0);
signal signal_sign : std_logic;
signal signal_threshold : std_logic_vector (15 downto 0);
signal par1 : std_logic_vector(63 downto 0);
signal par2 : std_logic_vector(63 downto 0);
signal par3 : std_logic_vector(63 downto 0);
signal par4 : std_logic_vector(63 downto 0);
signal OuterRingFactor : std_logic_vector(15 downto 0);
signal InnerRingFactor : std_logic_vector(15 downto 0);
signal PreScaling : std_logic_vector(15 downto 0);


begin

-- change active low into active high signals
	BF_AMS0	<= NOT BF_AMS0_b;
	BF_AMS1	<= NOT BF_AMS1_b;
	BF_WR		<= NOT BF_AWE_b;
	BF_RD		<= NOT BF_ARE_b;

-- logic for 'enable' signals
	BF_rena0	<= BF_AMS0 AND BF_RD AND (not BF_WR);
	BF_rena1	<= BF_AMS1 AND BF_RD AND (not BF_WR);
	BF_WRENA0	<= BF_AMS0 AND BF_WR AND (not BF_RD);
	BF_WRENA1	<= BF_AMS1 AND BF_WR AND (not BF_RD);
	BF_WRENA0_v(0) <= BF_WRENA0;
	
-- address decoder AMS0
	MemSelect <=																		-- result addr
	"0001" WHEN BF_AMS0& BF_ADDR (25 downto 14)="1000000000000" ELSE	-- 0x20000000
	"0010" WHEN BF_AMS0& BF_ADDR (25 downto 14)="1000001000000" ELSE	-- 0x20100000
	"0010" WHEN BF_AMS0& BF_ADDR (25 downto 14)="1000001000000" ELSE	-- 0x20100000
	"0100" WHEN BF_AMS0& BF_ADDR (25 downto 14)="1000010000000" ELSE	-- 0x20200000
	"1000" WHEN BF_AMS0& BF_ADDR (25 downto 14)="1000011000000" ELSE	-- 0x20300000
	(others => '0');
	
-- address decoder CONTROL REGISTER
	RawSelect <=
	"0001" WHEN BF_ADDR (25 downto 2)="000000000000000000000001" ELSE
	"0010" WHEN BF_ADDR (25 downto 2)="000000000000000000000010" ELSE
	"0100" WHEN BF_ADDR (25 downto 2)="000000000000000000000011" ELSE
	"1000" WHEN BF_ADDR (25 downto 2)="000000000000000000000100" ELSE
	(others => '0');
	RegSelect <= RawSelect WHEN (BF_AMS0 = '0') AND (BF_AMS1 = '1') -- LOW = '0' ?? HIGH = '1' ??
					ELSE (others => '0');

	
	-- Multiplexer
	MemorySpace0 <=
		BF_MemBus(0) WHEN MemSelect="0001" ELSE
		BF_MemBus(1) WHEN MemSelect="0010" ELSE
		BF_MemBus(2) WHEN MemSelect="0100" ELSE
		BF_MemBus(3) WHEN MemSelect="1000" ELSE
		(others=>'Z');
	BF_DATA <= MemorySpace0 WHEN BF_rena0 = '1' ELSE (others=>'Z');


	-- initiate BRAM component
	BRAM0 : BRAM
		port map (
			-- PORT A, BF side
			clka	=> BF_PCB_CLK,
			ena 	=> MemSelect(0),
			wea 	=> BF_WRENA0_v,
			addra => BF_ADDR(13 downto 2),
			dina 	=> BF_DATA,
			douta => BF_MemBus(0),
			-- PORT B, FPGA side
			clkb 	=> ADC_PCB_CLK,
			enb 	=> enable_b,
			web 	=> wrenable_b,
			addrb => MemAddr_ctr,
			dinb 	=> input_b,
			doutb => output_b);
	MemAddr_ctr<=address_initiate + address_status;
	enable_b<=enable_initiate OR enable_status;


	initiate_parameters : INITIATE_COMPONENT
		PORT map(
			CLK_ADC	=> ADC_PCB_CLK,
			initiate => initiate,
			output_b => output_b,
			enable_b => enable_initiate,
			MemAddr_ctr => address_initiate,
			-- parameter
			signal_sign		=> signal_sign,
			int_window		=> int_window_vec,
			veto_delay		=> veto_delay_vec,
			signal_threshold => signal_threshold,
			int_threshold	=> int_threshold,
			width_cut		=> width_cut_vec,
			risetime_cut	=> risetime_cut_vec,
			component_selector => component_selector_vec,
			par1 => par1,			
			par2 => par2,
			par3 => par3,
			par4 => par4,
			OuterRingFactor => OuterRingFactor,
			InnerRingFactor => InnerRingFactor,
			PreScaling => PreScaling
	);
	int_window <= CONV_INTEGER(int_window_vec);
	width_cut <= CONV_INTEGER(width_cut_vec);
	risetime_cut <= CONV_INTEGER(risetime_cut_vec);
	veto_delay <= CONV_INTEGER(veto_delay_vec);
	component_selector <= component_selector_vec(3 downto 0);
	


	-- initiate CONTROL REGISTER
	BFcontrol: CTRL_reg_CPU_writes -- BF control over FPGA
		GENERIC MAP (regWdt => 16)
		PORT MAP (
			CLK=>BF_PCB_CLK,
			CS=>RegSelect(0),
			WR=>BF_WRENA1,
			RD=>BF_rena1,
			IOBUS=>BF_DATA (15 downto 0),
			regout=>BF_ctrl_Reg (15 downto 0));
			
	get_baseline <= BF_ctrl_Reg(0);
	initiate <= BF_ctrl_Reg(1);
	get_status <= BF_ctrl_Reg(2);
	LED_TestFlash <= BF_ctrl_REG(3);


	veto_status : STATUS_COMPONENT
		PORT map(
			CLK_ADC	=> ADC_PCB_CLK,
			get_status => get_status,
			input_b => input_b,
			enable_b => enable_status,
			wrenable_b => wrenable_b,
			MemAddr_ctr => address_status,
			-- parameter
			signal_sign		=> signal_sign,
			int_window		=> int_window,
			veto_delay		=> veto_delay,
			signal_threshold => signal_threshold,
			int_threshold => int_threshold,
			width_cut		=> width_cut,
			risetime_cut	=> risetime_cut,
			component_selector => component_selector,
			par1 => par1,
			par2 => par2,
			par3 => par3,
			par4 => par4,
			OuterRingFactor => OuterRingFactor,
			InnerRingFactor => InnerRingFactor,
			PreScaling => PreScaling,
			baseline	=> baseline,
			baselineOuterRing => baselineOuter,
			baselineInnerRing => baselineInner
	);

---- Debounce and baseline for sum signal-----
	DEB_DEL: DEBOUNCER
		port map(
			clk=>ADC_PCB_CLK,
			ADC_in=>AD0_A,
			ADC_out=>adc0_16);


	BLine_calculator : BLINE
		PORT MAP (
			clk=>ADC_PCB_CLK,
			getBline=>get_baseline, -- get baseline when ctrl_reg(2) := '1'
			initiate=>initiate, -- get baseline when initiation is '1'
			signal_sign=>signal_sign,
			ADC_in=>adc0_16,
			ADC_out=>adc0_16_corr,
			baseline=>baseline,
			baseline_ok=>baseline_ok
			);
------------------------------------------------
	
	PeakInfo : PEAK_INFO_COMPONENT
		PORT MAP (
		clk=>ADC_PCB_CLK,
		ADC_All=>adc0_16_corr,
		GetBLine=>get_baseline,
		Initiate=>initiate,
		Baseline=>baseline,
		BaselineOK=>baseline_ok,
		SignalSign=>signal_sign,
		IntWindow=>int_window,
		SignalThreshold=>signal_threshold,
		PeakStart=>start_peak,
		PeakIntegral=>integrated_sig,
		PeakWidth=>peak_width,
		PeakRiseTime=>peak_risetime
	);
	
	RadialPosition : RADIAL_POSITION_COMPONENT
	PORT MAP (
	clk=>ADC_PCB_CLK,
	ADC_AllCorr=>adc0_16_corr,
	ADC_OuterRing=>AD0_B,
	ADC_InnerRing=>AD1_A,
	GetBLine=>get_baseline,
	Initiate=>initiate,
	SignalSign=>signal_sign,
	IntWindow=>int_window,
	SignalThreshold=>signal_threshold,
	OuterRingFactor=>OuterRingFactor,
	InnerRingFactor=>InnerRingFactor,
	RingVetoOut=>RingVetoOut,
	RingVetoTest=>RingVetoTest,
	RingVetoPeakOn=>RingVetoPeakOn,
	baselineOuter=>baselineOuter,
	baselineOuter_ok=>baselineOuter_ok,
	baselineInner=>baselineInner,
	baselineInner_ok=>baselineInner_ok
	);
	
	veto: VETO_COMPONENT
	-- delay
	PORT MAP(
		clk=>ADC_PCB_CLK,
		veto_delay=>veto_delay,
		ADC_in=>adc0_16_corr,
		component_selector=>component_selector,
		width_cut=>width_cut,
		risetime_cut=>risetime_cut,
		signal_threshold=>signal_threshold,
		threshold=>int_threshold,
		IntWindow=>int_window,
		integral=>integrated_sig,
		width=>peak_width,
		risetime=>peak_risetime,
		PositionVeto=>RingVetoOut,
		PreScaling=>PreScaling,
		par1 => par1,
		par2 => par2,
		par3 => par3,
		par4 => par4,
		test1 => test1,
		test2 => test2,	
		test3 => test3,
		veto=>veto_out,
		start_peak=>start_peak,
		start_veto=>start_veto
	);
	
	status_LED : STATUS_LED_COMPONENT
	PORT MAP(
	clk=>ADC_PCB_CLK,
	initiate=>initiate,
	LED_TestFlash=>LED_TestFlash,
	RingVetoIn=>RingVetoOut,
	HEvetoIn=>veto_out,
	InputSignal=>adc0_16,
	component_selector=>component_selector,
	SumSigPeakOn=>start_peak,
	RingVetoPeakOn=>RingVetoPeakOn,
	Saturation=>Saturation,
	LED_out=>LED_out
	);

-------------------------------
---------- OUTPUTS ------------

	LED(0)<=LED_out(0); -- status of risetime component
	LED(1)<=LED_out(1); -- status of width component
	LED(2)<=LED_out(2); -- status of integral component
	LED(3)<=LED_out(3); -- status of Ring veto component
		
	NIM_OUT(0)<= Saturation; -- is on if sum signal is saturated (input > "1111111111111100". not sure if this works...)
	NIM_OUT(1)<= start_peak; -- is on if signal is above threshold on sum signal
	NIM_OUT(2)<= RingVetoPeakOn; -- is on if signal of input 1+2 is above threshold
	
	------------------------------------------
	NIM_OUT(3)<=veto_out; -- final veto output
	------------------------------------------

end Behavioral;
