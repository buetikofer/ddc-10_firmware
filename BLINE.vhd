----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    13:10:38 10/30/2013 
-- Design Name: 
-- Module Name:    BLINE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 	Calculates baseline based on the average of 64 adc samples (640ns).
--						The component returns the value of the average (baseline) as well as a controlsignal, that 
--						the baseline has been calculated.
--						If there are no dark counts, it takes about 1 us to get the baseline
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BLINE is
	port (
	clk:			in std_logic;									-- ADC clock
	getBline:	in std_logic;
	initiate :  in std_logic;
	signal_sign:in std_logic;
	ADC_in:		in std_logic_vector(15 downto 0); 	-- signal input
	ADC_out:		out std_logic_vector(15 downto 0);		-- signal output (baseline corrected if available)
	baseline:	out std_logic_vector(15 downto 0);
	baseline_ok:out std_logic := '0'
);
end BLINE;

architecture Behavioral of BLINE is

signal sum : std_logic_vector(21 downto 0) := (others => '0');
signal average : std_logic_vector(15 downto 0) := (others => '0');
signal counter : integer range 0 to 64 := 0;
signal sum_ok	: std_logic := '0';
signal baseline_done : std_logic := '0';
signal temp_adc1 : std_logic_vector(15 downto 0) := (others => '0');
signal temp_adc2 : std_logic_vector(15 downto 0) := (others => '0');
signal temp_adc3 : std_logic_vector(15 downto 0) := (others => '0');

begin

	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
		
		
			IF getBline = '1' OR initiate = '1' THEN
				counter<=0;
				sum_ok<='0';
				baseline_done<='0';
				average<=(others => '0');
				sum<=(others => '0');
			END IF;
		
			IF sum_ok = '0' and counter < 64 THEN 
				-- set counter to zero if there is a signal
				IF ADC_in < temp_adc3 THEN
					IF (temp_adc3 - ADC_in) > 100 THEN
						counter<=0;
						temp_adc1<=ADC_in;
						temp_adc2<=temp_adc1;
						temp_adc3<=temp_adc2;
						sum<=(others => '0');
					ELSE 
						counter<=counter + 1;
						sum<=sum +ADC_in;
						temp_adc1<=ADC_in;
						temp_adc2<=temp_adc1;
						temp_adc3<=temp_adc2;
					END IF;
				END IF; -- ADC_in < temp_adc
				IF ADC_in >= temp_adc3 THEN
					IF (ADC_in - temp_adc3) > 100 THEN
						counter<=0;
						temp_adc1<=ADC_in;
						temp_adc2<=temp_adc1;
						temp_adc3<=temp_adc2;
						sum<=(others => '0');
					ELSE 
						counter<=counter + 1;
						sum<=sum + ADC_in;
						temp_adc1<=ADC_in;
						temp_adc2<=temp_adc1;
						temp_adc3<=temp_adc2;
					END IF;
				END IF; -- ADC_in >= temp_adc
			END IF; -- get baseline
			
			IF counter = 64 THEN
				sum_ok<='1';
			END IF;
			
			-- division by 64 not very elaborated yet (seems OK)
			IF sum_ok = '1' AND baseline_done = '0' THEN
				baseline_done<='1';
				IF sum(5)='0' THEN 
					average<=sum(21 downto 6); -- round off
				ELSE
					average<=sum(21 downto 6) + "0000000000000001"; -- round up
				END IF;
			END IF;

			
			-- substract baseline, if available and invert signal if it is posivive (signal_sign = '1')
			IF signal_sign = '0' THEN
				IF baseline_done = '1' THEN
					ADC_out<=ADC_in - average; -- - average; 
					baseline<=average;
					baseline_ok<='1';
				ELSE
					ADC_out<=ADC_in;
					baseline<=(others=>'0');
					baseline_ok<='0';
				END IF;
			END IF;
			-- invert signal if it is positive (signal_sign = '1')
			IF signal_sign = '1' THEN
				IF baseline_done = '1' THEN
					ADC_out<=(not ADC_in) + average + 1; -- - average; 
					baseline<=average;
					baseline_ok<='1';
				ELSE
					ADC_out<=(not ADC_in) + 1;
					baseline<=(others=>'0');
					baseline_ok<='0';
				END IF;
		
			END IF;

						
		END IF; -- CLK 
	END PROCESS;	
	

end Behavioral;

