--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:30:04 11/13/2013
-- Design Name:   
-- Module Name:   /home/buetikofer/Documents/xenon/DDC-10/firmware/HE_veto1.2/tb_integrator.vhd
-- Project Name:  HE_veto1.2
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: INTEGRATOR
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_integrator IS
END tb_integrator;
 
ARCHITECTURE behavior OF tb_integrator IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT INTEGRATOR
    PORT(
         clk : IN  std_logic;
         enable : IN  std_logic;
			int_window: in integer range 0 to 1000;
         ADC : IN  std_logic_vector(15 downto 0);
         baseline : IN  std_logic_vector(15 downto 0);
         integral : OUT  std_logic_vector(21 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal enable : std_logic := '1';
   signal int_window : integer range 0 to 1000 := 120;
   signal ADC : std_logic_vector(15 downto 0) := (others => '0');
   signal baseline : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal integral : std_logic_vector(21 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: INTEGRATOR PORT MAP (
          clk => clk,
          enable => enable,
          int_window => int_window,
          ADC => ADC,
          baseline => baseline,
          integral => integral
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		ADC<="0000000000000001";
      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
