----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    18:06:01 12/06/2013 
-- Design Name: 
-- Module Name:    RISETIME_COMPONENT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 					This component calculates the rise time of the peak. The output of the risetime
--										is updated continuously. Maybe this has to be changed in order to take into account
--										second peaks, that apear within the veto delay.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;
--use IEEE.std_logic_arith.all;



-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RISETIME_COMPONENT is
	port (
	clk:			in std_logic;								-- ADC clock
	baseline_ok:in std_logic;
	ADC:			in std_logic_vector(15 downto 0);
	signal_threshold: in std_logic_vector(15 downto 0);
	risetime: 		out integer range 0 to 500
);
end RISETIME_COMPONENT;

architecture Behavioral of RISETIME_COMPONENT is
signal peak_on : std_logic := '0';
signal maximum : std_logic_vector(15 downto 0);
signal counter : integer range 0 to 500;
 
begin
	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
		
			IF ADC < (-signal_threshold) AND peak_on = '0' THEN 
				maximum<=ADC;
				counter<=1; 
				peak_on<='1';
			END IF;
			IF ADC > (-signal_threshold) THEN
				peak_on<='0';
			END IF;

			IF peak_on = '1' THEN
				counter <= counter + 1;
				IF ADC < maximum THEN
					maximum<=ADC;
					risetime<=counter;
				END IF;
			END IF;

		END IF; -- CLK 
	END PROCESS;


end Behavioral;

