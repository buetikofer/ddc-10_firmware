----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    10:13:24 11/19/2013 
-- Design Name: 
-- Module Name:    WIDTH - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 			At the moment the speed of the WIDTH COMPONENT is not maximised. Could be improved by 
--								calculating the peak position and start with the search for left_edge at this time, not after right_edge has been found.
--
--								Problem with very narrow peaks not solved!!!
--								Coded a way around (with if statement) -> line ~140
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_signed.ALL;
use IEEE.std_logic_arith.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity WIDTH_COMPONENT is
	port (
	clk:		in std_logic;								-- ADC clock
	baseline_ok: in std_logic;
	peak_start: in std_logic;
	ADC:		in std_logic_vector(15 downto 0);
	signal_threshold: in std_logic_vector(15 downto 0);
--	test4: 	out std_logic;
	width: 	out integer range 0 to 500
	);
end WIDTH_COMPONENT;

architecture Behavioral of WIDTH_COMPONENT is
TYPE ADC_MemBuffer IS ARRAY (500 downto 0) OF std_logic_vector (15 DOWNTO 0); -- Buffer for ADC samples

----- SIGNALS -------
signal maximum : std_logic_vector(15 downto 0);
signal half_maximum : std_logic_vector(14 downto 0);
signal counter : integer range 0 to 500;
--signal left_edge : integer range 0 to 500 := 1;
signal right_edge : integer range 0 to 500 := 1;

signal right_ok : std_logic := '1';
signal left_ok : std_logic := '1';

--signal width_int : integer range 0 to 500;
signal adc_buffer : ADC_MemBuffer := (others => "0000000000000000");
signal test : std_logic := '0';

signal width_temp : integer range 0 to 500 := 0;
signal send_width : std_logic :='0';


begin

	PROCESS (clk) BEGIN
		IF rising_edge (clk) THEN
		
			IF baseline_ok = '0' THEN
				test <= '0';
				left_ok <= '1';
				right_ok <= '1';
				counter <= 0;
			END IF;
						
					
			IF baseline_ok = '1' THEN
				IF ADC < (-signal_threshold) AND test = '0' AND left_ok = '1' AND right_ok = '1' THEN 
					maximum <= ADC;
					half_maximum <= ADC(15 downto 1);
					adc_buffer(counter) <= ADC; 
					counter <= counter + 1; -- set counter to 1
					--left_edge <= 0;
					right_edge <= 0;				
					right_ok <= '0';
					left_ok <= '0';				
					test<='1';
				END IF;
				IF ADC > (-signal_threshold) THEN
					test<='0';
				END IF;
				
				
				IF left_ok = '0' AND right_ok = '0' THEN -- get right edge
					adc_buffer(counter)<=ADC;
				
					-- only negative peaks
					IF ADC < maximum THEN
						maximum <= ADC;
						half_maximum <= ADC(15 downto 1);
					END IF;
					
					IF ADC > half_maximum AND right_ok = '0' THEN
						right_edge<=counter;
						right_ok <= '1';
						counter <= 0;
					ELSE
						counter<=counter + 1;
					END IF;

				END IF; -- left_edge = 0 AND right_edge = 0
				
				
				
				IF left_ok = '0' AND right_ok = '1' THEN -- get left edge
					-- negative peaks
						IF adc_buffer(counter) < half_maximum THEN
							--left_edge<=counter;
							left_ok <= '1';
							send_width<='1';
							-- 
							-- There is a problem with small and very narrow pulses. 
							-- Somehow 'right_edge' gets smaller then left_edge (i.e. counter), therefore width
							-- gets negative an jumps to ~512. 
							-- Since this happens only for small and narrow pulses the width is artificially set to 1 sample
							-- for these cases.
							-- The reason for this bug is not found yet.
							--
							IF (right_edge - counter) < 0 THEN
								width_temp<=1;
							ELSE
								width_temp<=right_edge-counter;
							END IF;
							counter<=0;
						ELSE 
							counter<=counter + 1;
						END IF;				
				END IF; -- get left edge
				
				--width should be zero unless a new width has been found					
				IF send_width = '1' THEN
					width<=width_temp;
					send_width<='0';
				END IF;
				IF send_width = '0' THEN
					width<=0;
				END IF;

				--test4<=send_width;
				
			END IF; -- baseline_ok
		END IF; -- CLK 
	END PROCESS;

end Behavioral;

