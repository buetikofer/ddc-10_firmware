----------------------------------------------------------------------------------
-- Company: LHEP Universität Bern
-- Engineer: Lukas Bütikofer
-- 
-- Create Date:    14:53:00 10/01/2013 
-- Design Name: 
-- Module Name:    CTRL_reg_CPU_writes - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- CTRL_reg_CPU_writes.vhd. Aregister Blackfin --> FPGA.
-- This register can be written and read back by the CPU.
-- It does not read data from the FPGA fabric.
-- It can only read back the data previously written to it.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Our own types, constants and components in file BlackVME_types.vhd
--use work.BlackVME_types.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CTRL_reg_CPU_writes is
	GENERIC (regWdt: INTEGER :=16);
	PORT (
	CLK	: in STD_LOGIC;
	CS	: in STD_LOGIC;	-- selection
	WR	: in STD_LOGIC;	-- write enable
	RD	: in STD_LOGIC;	-- read enable
	IOBUS	: inout STD_LOGIC_VECTOR (regWdt-1 downto 0);	-- data BUS (BF_DATA)
	regout	: out STD_LOGIC_VECTOR (regWdt-1 downto 0));	-- output
end CTRL_reg_CPU_writes;

architecture Behavioral of CTRL_reg_CPU_writes is
SIGNAL rena, wrena: STD_LOGIC;
SIGNAL local: STD_LOGIC_VECTOR(regWdt-1 downto 0);


begin -- ARCHITECTURE IMPLEMENTATION
wrena <= '1' WHEN ((CS='1') AND (WR='1') AND (RD='0')) ELSE '0';
rena <= '1' WHEN ((CS='1') AND (WR='0') AND (RD='1')) ELSE '0';

IOBUS <= local WHEN rena = '1' ELSE (others => 'Z');

make_register: PROCESS (CLK, wrena) BEGIN
	IF rising_edge(CLK) THEN
		IF (wrena = '1') THEN
			local <= IOBUS;	-- from Blackfin to fabric
		END IF;
	END IF;	-- CLK
END PROCESS make_register;

regout <= local; -- static output from the register to controlled logic
end Behavioral;

